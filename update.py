import mysql.connector
import csv

CSV_FILE = 'CognitoUsers.csv'

def update_uuid(user_id, email):
    """UPDATE local DB columns """
    try:
        conn = mysql.connector.connect(user='root', password='root', host='localhost', database='andrius_recar')
        cursor = conn.cursor()

        # disable foreigh key check
        set_check = """SET FOREIGN_KEY_CHECKS = 0"""
        cursor.execute(set_check)
        conn.commit()

        # update user_company table
        update_user_company = """UPDATE user_company uu INNER JOIN user u ON uu.user_id=u.id 
        SET uu.user_id=uuid_to_bin(%s) WHERE u.email= %s"""
        input_data = (user_id, email)
        cursor.execute(update_user_company, input_data)
        conn.commit()

        # update user_department table
        update_user_department = """UPDATE user_department uu INNER JOIN user u ON uu.user_id=u.id 
        SET uu.user_id=uuid_to_bin(%s) WHERE u.email= %s"""
        input_data = (user_id, email)
        cursor.execute(update_user_department, input_data)
        conn.commit()

        # update user_distributor table
        update_user_distributor = """UPDATE user_distributor uu INNER JOIN user u ON uu.user_id=u.id 
        SET uu.user_id=uuid_to_bin(%s) WHERE u.email= %s"""
        input_data = (user_id, email)
        cursor.execute(update_user_distributor, input_data)
        conn.commit()

        # update user_role table
        update_user_role = """UPDATE user_role uu INNER JOIN user u ON uu.user_id=u.id 
        SET uu.user_id=uuid_to_bin(%s) WHERE u.email= %s"""
        input_data = (user_id, email)
        cursor.execute(update_user_role, input_data)
        conn.commit()

        update_user = """UPDATE user SET id=uuid_to_bin(%s) where email = %s"""
        input_data = (user_id, email)
        cursor.execute(update_user, input_data)
        conn.commit()

        set_check_on = """SET FOREIGN_KEY_CHECKS = 1"""
        cursor.execute(set_check_on)
        conn.commit()

        print("UPDATE successfull")

    except mysql.connector.Error as error:\
        print(f"Failed to UPDATE DB. Error message {error}")
    finally:
        if conn.is_connected():
            cursor.close()
            conn.close()
            print("MySQL Connection closed")


def main():
    with open(CSV_FILE, mode='r') as cognito_user:
        csv_reader = csv.reader(cognito_user, delimiter=",")

        for line in csv_reader:
            user_id = line[0]
            email = line[1]
            update_uuid(user_id, email)


if __name__ == '__main__':
    main()
