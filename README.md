**README**

This README document steps are necessary to update local DB user uuid with values from AWS User Pool client.

---

## What is this repository for?

1. To update local db user uuid with new values
2. Save DB `user` relationships with `user_company`, `user_department`, `user_distributor`, `user_role`
3. repository is needed to complete https://recaras.atlassian.net/wiki/spaces/RECAR/pages/352059393/Running+Recar+locally

---

## How do I get set up?

1. Create virtualenvironment at repository
2. Activate virtualenvironment
3. Install requirements `pip install -r requirements.txt`

---

## Who do I talk to?

* User who need to update local db `user.id`